import { google } from 'googleapis';

const sheets = google.sheets('v4');

export const getSheetData = async () => {

  const auth = new google.auth.JWT(
    process.env.GOOGLE_SHEETS_CLIENT_EMAIL,
    null,
    process.env.GOOGLE_SHEETS_PRIVATE_KEY.replace(/\\n/g, '\n'),
    ['https://www.googleapis.com/auth/spreadsheets']
  );

  const response = await sheets.spreadsheets.values.get({
    auth,
    spreadsheetId: process.env.GOOGLE_SHEETS_SPREADSHEET_ID,
    range: 'Sheet1!A1:D5', // 修改這裡以適應你的範圍
  });
  return response.data.values.reverse();
};
export const addSheetData = async (row) => {
  const auth = new google.auth.JWT(
    process.env.GOOGLE_SHEETS_CLIENT_EMAIL,
    null,
    process.env.GOOGLE_SHEETS_PRIVATE_KEY.replace(/\\n/g, '\n'),
    ['https://www.googleapis.com/auth/spreadsheets']
  );

  const response = await sheets.spreadsheets.values.append({
    auth,
    spreadsheetId: process.env.GOOGLE_SHEETS_SPREADSHEET_ID,
    range: 'Sheet1!A:E', // 根據你的需求調整範圍
    valueInputOption: 'RAW',
    resource: {
      values: [row],
    },
  });

  return response.status;
};