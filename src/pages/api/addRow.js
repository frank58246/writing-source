import { addSheetData } from '../../lib/googleSheets';

export default async function handler(req, res) {
  if (req.method === 'POST') {
    const { row } = req.body;

    try {
      const response = await addSheetData(row);
      res.status(200).json({ status: 'success', response });
    } catch (error) {
      res.status(500).json({ status: 'error', error: error.message });
    }
  } else {
    res.status(405).json({ status: 'error', message: 'Method not allowed' });
  }
}
