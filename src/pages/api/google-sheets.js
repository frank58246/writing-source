// pages/api/google-sheets.js

import { google } from 'googleapis';

const handler = async (req, res) => {
  try {
    // 使用Google Sheets API获取数据的代码示例
    const auth = new google.auth.JWT(
        process.env.GOOGLE_SHEETS_CLIENT_EMAIL,
        null,
        process.env.GOOGLE_SHEETS_PRIVATE_KEY.replace(/\\n/g, '\n'),
        ['https://www.googleapis.com/auth/spreadsheets']
      );


    const sheets = google.sheets({ version: 'v4', auth: auth });

    // 根据需要执行查询或获取数据的操作
    const response = await sheets.spreadsheets.values.get({
      spreadsheetId: '1g0BPMNIe3c0CJFFpR1jtS6Y7dEvutu9p1zOtzeFrRpU',
      range: 'Sheet1!A1:B10000', // 根据你的表格设置
    });

    const rows = response.data.values;
    res.status(200).json(rows);
  } catch (error) {
    console.error('Error fetching data from Google Sheets:', error);
    res.status(500).json({ error: 'Failed to fetch data' });
  }
};

export default handler;
