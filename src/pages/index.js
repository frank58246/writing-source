import { useEffect, useState } from 'react';
import { getSheetData } from '../lib/googleSheets';
import styles from '../styles/Home.module.css';

export async function getServerSideProps() {
  const sheetData = await getSheetData();

  return {
    props: {
      sheetData,
    },
  };
}

export default function Home({  }) {
  const [newData, setNewData] = useState(['', '', '', '']); // 根據需要調整初始值
  const [message, setMessage] = useState('');
  const [sheetData, setSheetData] = useState([]);

  const handleInputChange = (index, value) => {
    const updatedData = [...newData];
    updatedData[index] = value;
    setNewData(updatedData);
  };
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('/api/google-sheets');
        if (response.ok) {
          const json = await response.json();
          setSheetData(json);
        } else {
          console.error('Failed to fetch data from Google Sheets API');
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []);
  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const response = await fetch('/api/addRow', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ row: newData }),
      });

      if (response.ok) {
        setMessage('Data added successfully!');
      } else {
        setMessage('Failed to add data.');
      }
    } catch (error) {
      setMessage('Error: ' + error.message);
    }
  };
   function renderAdd()    {
    return sheetData.length > 0 && (
      <form onSubmit={handleSubmit} className={styles.form}>
        {sheetData[0].map((header, index) => (
          <div key={index} className={styles.formGroup}>
            <input
              type="text"
              value={newData[index]}
              onChange={(e) => handleInputChange(index, e.target.value)}
              className={styles.input}
            />
          </div>
        ))}
        <button type="submit" className={styles.button}>Add Data</button>
      </form>
      
    )
  }

  function renderTable(){
    return sheetData.length > 0 && (
      <table className={styles.table}>
        <thead>
          <tr>
            {sheetData[sheetData.length - 1].map((header, index) => (
              <th key={index}>{header}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {sheetData.slice(0, sheetData.length - 1).map((row, index) => (
            <tr key={index}>
              {row.map((cell, cellIndex) => (
                <td key={cellIndex}>{cell}</td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    )
  }
  return (
    <div className={styles.container}>
      <h1>Add New Data</h1>
       {renderAdd()}

      <h1 className={styles.title}>Google Sheets Data</h1>
      {renderTable()}  
    </div>
  );
}
